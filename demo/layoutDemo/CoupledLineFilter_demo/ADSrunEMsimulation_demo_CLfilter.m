clear variables
close all
clc

cd(fileparts(mfilename('fullpath')));

% CLfilter_demo.net contains is an ADS netlist that contains several
% instances of transmission lines. The ADScreateLayout function is called
% to create the layout file. The warning about the Port component from
% ASDScreateLayout can be ignored 


% generate the default options struct for momentum simulations
options = ADSgenerateOptFile();
% change the number of cells per wavelength
options.no_cells_per_wavelength = 10;

% run the simulation, with the following bonus options
%   show the generated lay-out with the mesh
%   override the frequency grid with 'fstart', 'fstop' and 'dec'
%   use the custom momentum options specified in the options struct
res  = ADSrunEMsimulation('CLfilter_demo.net','showGeneratedLayout',true,...
    'fStart',5.5e9,'fStop',6.5e9,'fStep',50e6,'optSettings',options);

%% plot the simulation result
figure(101)
clf
hold on
for uu=1:size(res.S,1);
    for yy=1:size(res.S,2)
        plot(res.freq,db(squeeze(res.S(uu,yy,:))));
    end
end
legend(leg)
