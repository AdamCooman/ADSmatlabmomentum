function polygonText = writePolygonText(polygons,unit)
%writePolygonText.m makes a cell array polygonText that can be used by
%writeTextFile.m to create a text a file in this case to write the proj_a file. 
%The required fields are:
%   unit    This string specifies in which units the polygons is given: M
%           (meter) of MM (millimeter).
%
%   polygons    This array of structures contains the polygons that are
%               already created. It has following fields:
%               polygons(i).Coord: this contains the coordinates of the
%               polygon.
%               polygons(i).Layer: this contains the number of the layer.
%               The toplayer has number 1 or has no number.
%19/02/2014
%matthias ELEC

Npoly=length(polygons);

polygonText{1}='UNITS MM,10000;';
polygonText{2}='EDIT proj;';
n1=3;

for n=1:Npoly
    switch unit
        case 'M'
            polygons(1,n).Coord=polygons(1,n).Coord*1e3;
        case 'MM'
        otherwise
            error('unknown unit')
    end
     
    if ~isempty(polygons(1,n).Coord)
        nPoints=length(polygons(1,n).Coord);
        
        switch polygons(1,n).Layer
            case 1
                string='ADD P1 :W0.000000 ';
            case 2
                string='ADD P2 :W0.000000 ';
        end
        
        for nP=1:nPoints
            string=[string [' ' num2str(polygons(1,n).Coord(nP,1)) ',' num2str(polygons(1,n).Coord(nP,2))]];
        end
        
        string=[string ';'];
        polygonText{n1}=string;
        n1=n1+1;
    end
end

polygonText{n1}='SAVE;';


end

