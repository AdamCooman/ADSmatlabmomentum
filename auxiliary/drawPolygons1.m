function drawPolygons(polygons,nodes)
%drawPolygons.m  plots the figure corresponding to the given array of
%polygon structures. The function can be used to check wheter the created
%polygons correspond the the desired layout. Also there is a difference
%between the layers.
%Matthias Caenepeel, ELEC
%18/02/2014 V1


Npoly=length(polygons);

figure
for n1=1:Npoly
    
    try
        poly=polygons(n1).Coord;
        switch (polygons(n1).Layer)
            case(1)
                plot([poly(1:end,1);poly(1,1)],[poly(1:end,2);poly(1,2)],'-b','displayName','Layer 1')
                hold on
            case(2)
                plot([poly(1:end,1);poly(1,1)],[poly(1:end,2);poly(1,2)],'-r','displayName','Layer 2')
                hold on

        end
    catch ME
    end
end

nodeMark = {'*k','*r','*b','*c','*b','*g'};
k1 = 1;

if exist('nodes','var')
    for i1=1:length(nodes)
        plot(nodes(i1).x,nodes(i1).y,nodeMark{k1},'displayName',['Node ' nodes(i1).Name])
        k1 = k1+1;
        if k1>length(nodeMark)
            k1 = 1;
        end
            
    end
end
xlabel('x coordinate (m)')
ylabel('y coordinate (m)')
legend('show')
axis('equal')


end

