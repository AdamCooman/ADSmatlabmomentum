function NetlistCell = ADSprintNetlist(NetlistStruct,file)
% ADSPRINTNETLIST converts a NetlistStruct to a file which can be read by hpeesofsim
%
%   res = ADSprintNetlist(NetlistStruct,filename)
%
% where NetlistStruct is a structure array which contains the statements of the netlist
% Info about the NetlistStruct can be found in ADSparseNetlist
% file can be a string which contains the filename or the file identifier of the file to write to.
%
%
% Adam Cooman, ELEC VUB

NetlistCell = NetlistStruct2string(NetlistStruct);

if ~exist('file','var')
    file = -1;
end

try %#ok
    writeTextFile(Text,file);
end

end


function NetlistCell = NetlistStruct2string(NetlistStruct)
NetlistCell={};
for ii=1:length(NetlistStruct)
    switch NetlistStruct(ii).StatementType
        case 'INSTANCE'
            statement = {instance2string(NetlistStruct(ii))};
        case 'SUBCIRCUIT'
            statement = subcircuit2string(NetlistStruct(ii));
        case 'VARIABLE'
            statement = {variable2string(NetlistStruct(ii))};
        case 'COMMENT'
            statement = {comment2string(NetlistStruct(ii))};
        case 'GLOBALNODE'
            statement = {globalnode2string(NetlistStruct(ii))};
        case 'MODEL'
            statement = {model2string(NetlistStruct(ii))};
        case 'ADS_C_PREPROCESSOR'
            statement = {preprocessor2string(NetlistStruct(ii))};
        case 'OPTIONS'
            statement = {options2string(NetlistStruct(ii))};
        otherwise
            error('Unknown StatementType');
    end
    NetlistCell(end+1:end+length(statement),1) = statement;
end
end


%% translate instance statement to string
function res = instance2string(in)
    res = sprintf('%s: %s %s %s',in.Type,in.Name,nodes2string(in.Nodes),parameters2string(in.Parameters));
end

%% translate subcircuit statement to strings
function res = subcircuit2string(in)
    res{1,1} = sprintf('define %s ( %s )',in.Name,nodes2string(in.Nodes));
    if ~isempty(fieldnames(in.Parameters))
        res{2,1} = sprintf('parameters %s',parameters2string(in.Parameters));
        NetlistCell = NetlistStruct2string(in.Statements);
        res(3:2+length(NetlistCell),1)=NetlistCell;
    else
        NetlistCell = NetlistStruct2string(in.Statements);
        res(2:1+length(NetlistCell),1)=NetlistCell;
    end
    res{end+1,1}=sprintf('end %s',in.Name);
end

%% translate model statement to string
function res = model2string(in)
    res = sprintf('model %s %s %s',in.Name,in.Type,parameters2string(in.Parameters));
end

%% translate globalnode statement to string
function res = globalnode2string(in)
    res = sprintf('globalnode %s',nodes2string(in.Nodes));
end

%% translate comment to string
function res = comment2string(in)
    res = sprintf('; %s',in.Value);
end

%% translate options statement to string
function res = options2string(in)
    res = sprintf('options %s',parameters2string(in.Parameters));
end

%% translate preprocessor statement to string
function res = preprocessor2string(in)
    res = sprintf('# %s',in.Value);
end

%% translate variable to string
function res = variable2string(in)
    res = sprintf('%s=%s',in.Name,in.Value);
end

%% utility function to translate a list of Nodes into a string
function res = nodes2string(Nodes)
res = '';
if ~isempty(Nodes)
    for ii=1:length(Nodes)
        res = [res ' ' Nodes{ii}];
    end
end
end

%% utility function to translate a list of parameters and values into a string
function res = parameters2string(Parameters)
fields = fieldnames(Parameters);
res = '';
for ii=1:length(fields)
    res = [res ' ' genvarnameInv(fields{ii}) '=' Parameters.(fields{ii})];
end
end



