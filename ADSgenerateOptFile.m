function [args,filestr] = ADSgenerateOptFile(varargin)
% ADSgenerateOptFile generates the default simulation settings for momentum
% 
%      [args,filestr] = ADSgenerateOptFile();
%      [args,filestr] = ADSgenerateOptFile('ParamName',paramValue,...);
% 
% The function generates a struct with the different simulation options for the meshing
% and solving of a momentum simulation. You can pass it without arguments to obtain
% a default struct with the parameters, you can the edit that struct to the function
% to write the .opt file
% 
% 
% Parameter/Value pairs:
% - 'simulationMode'     default: 1  check: @isscalar
%      
% - 'logFile'     default: 'proj.log'  check: @ischar
%      name of the log file the simulator info is written to
% - 'processPriority'     default: 'Normal'  check: @ischar
%      process priority given to the momentum process. Options are
%      Normal
% - 'mesh_reduction'     default: 'OFF'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      indicates whether mesh reduction is turned ON or OFF while generating
%      the mesh
% - 'no_cells_per_wavelength'     default: 20  check: @isscalar
%      indicates the amount of cells per wavelength
% - 'edgemesh_borderwidth'     default: 0  check: @isscalar
%      width the edge mesh has from the border of the metal planes?
%      The units  are in metre. set it to 0 to have no edge mesh
% - 'mesh_convergence_plot'     default: 'OFF'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      some weird plot that can be made by momentum apparently, no
%      idea what it  does.
% - 'save_fields_for'     default: 'nofrequencies'  check: @ischar
%      
% - 'drcLogging'     default: 'ON'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      turns on the DRC logging. I have no idea what DRC logging does
%      though
% - 'drcLayer'     default: 255  check: @isscalar
%      Layer for the DRC rules
% - 'overlapExtraction'     default: 'ON'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      overlapextraction
% - 'gppMergeAllShapes'     default: 'ON'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      gppMergeAllShapes
% - 'gppSimplifyAbsTol'     default: 0.01  check: @isscalar
%      
% - 'gppSimplifyRelTol'     default: 0.082  check: @isscalar
%      
% - 'gppMinFeatureSize'     default: -0.5  check: @isscalar
%      
% - 'topoWireViasPadringRadius'     default: 3  check: @isscalar
%      
% - 'topoWireViasAntipadringRadius'     default: 5  check: @isscalar
%      
% - 'topoWireViasThermalRadius'     default: 5  check: @isscalar
%      
% - 'topoWireViasKeepViaOutline'     default: 'OFF'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      
% - 'topoWireViasKeepThroughPads'     default: 'OFF'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      
% - 'modelTypeStrip'     default: 3  check: @isscalar
%      
% - 'modelTypeVia'     default: 3  check: @isscalar
%      
% - 'saveCurrentsInFile'     default: 1  check: @isscalar
%      
% - 'DS_DIR'     default: ''  check: @ischar
%      
% - 'DS_NAME'     default: ''  check: @ischar
%      
% - 'reusePreviousResults'     default: 'OFF'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      
% - 'includePortSolver'     default: 'ON'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      
% - 'matrixSolver'     default: 0  check: @isscalar
%      
% 
% Outputs:
% - args      Type: struct
%      struct with the different default settings
% - filestr      Type: cell array of strings
%      contains the contents of the .opt file that can be used by momentum
% 
% Adam Cooman, ELEC, VUB
% 
% Version info:
%  21/01/2015 version 1
% 
% This documentation was generated with the generateFunctionHelp function at 21-Jan-2015

p = inputParser();
p.StructExpand = true;
% simulation mode, no idea what it means
p.addParamValue('simulationMode',1,@isscalar);

% name of the log file the simulator info is written to
p.addParamValue('logFile','proj.log',@ischar);

% process priority given to the momentum process. Options are Normal
p.addParamValue('processPriority','Normal',@ischar);

% indicates whether mesh reduction is turned ON or OFF while generating the mesh
p.addParamValue('mesh_reduction','OFF',@(x) strcmpi(x,'ON')||strcmpi(x,'OFF'));

% indicates the amount of cells per wavelength
p.addParamValue('no_cells_per_wavelength',20,@isscalar);
% p.addParamValue('no_cells_per_wavelength',[],@(x) isscalar(x));

% width the edge mesh has from the border of the metal planes? The units
% are in metre. set it to 0 to have no edge mesh
p.addParamValue('edgemesh_borderwidth','AUTO',@(x) isscalar(x)|ischar(x));

% some weird plot that can be made by momentum apparently, no idea what it
% does.
p.addParamValue('mesh_convergence_plot','OFF',@(x) strcmpi(x,'ON')||strcmpi(x,'OFF'));

% 
p.addParamValue('save_fields_for','nofrequencies',@ischar);

% turns on the DRC logging. I have no idea what DRC logging does though
p.addParamValue('drcLogging','ON',@(x) strcmpi(x,'ON')||strcmpi(x,'OFF'));

% Layer for the DRC rules
p.addParamValue('drcLayer',255,@isscalar);

% overlapextraction
p.addParamValue('overlapExtraction','ON',@(x) strcmpi(x,'ON')||strcmpi(x,'OFF'));

% gppMergeAllShapes
p.addParamValue('gppMergeAllShapes','ON',@(x) strcmpi(x,'ON')||strcmpi(x,'OFF'));

% in units of LAMBDA
p.addParamValue('gppSimplifyAbsTol',0.01,@isscalar);

% 
p.addParamValue('gppSimplifyRelTol',0.082,@isscalar);

%
p.addParamValue('gppMinFeatureSize',-0.5,@isscalar);

% in VIARADII
p.addParamValue('topoWireViasPadringRadius',3,@isscalar);

% in VIARADII
p.addParamValue('topoWireViasAntipadringRadius',5,@isscalar);

% in VIARADII
p.addParamValue('topoWireViasThermalRadius',5,@isscalar);

%
p.addParamValue('topoWireViasKeepViaOutline','OFF',@(x) strcmpi(x,'ON')||strcmpi(x,'OFF'));

%
p.addParamValue('topoWireViasKeepThroughPads','OFF',@(x) strcmpi(x,'ON')||strcmpi(x,'OFF'));

%
p.addParamValue('modelTypeStrip',3,@isscalar);

%
p.addParamValue('modelTypeVia',3,@isscalar);

%
p.addParamValue('saveCurrentsInFile',1,@isscalar);

%
p.addParamValue('DS_DIR','',@ischar);

%
p.addParamValue('DS_NAME','',@ischar);

%
p.addParamValue('reusePreviousResults','OFF',@(x) strcmpi(x,'ON')||strcmpi(x,'OFF'));

%
p.addParamValue('includePortSolver','ON',@(x) strcmpi(x,'ON')||strcmpi(x,'OFF'));

%
p.addParamValue('matrixSolver',0,@isscalar);

p.parse(varargin{:});
args = p.Results;



% if wanted, generate the output file
if nargout==2
    filestr = {};
    filestr{end+1} = sprintf('simulationMode %s;',num2str(args.simulationMode));
    filestr{end+1} = sprintf('logFile "%s";',args.logFile);
    filestr{end+1} = sprintf('processPriority %s;',args.processPriority);
    filestr{end+1} = sprintf('mesh_reduction %s;',args.mesh_reduction);
    filestr{end+1} = sprintf('no_cells_per_wavelength %s;',num2str(args.no_cells_per_wavelength));
%     disp(['No of cells is ' num2str(args.no_cells_per_wavelength)])
    if ischar(args.edgemesh_borderwidth)
        filestr{end+1} = sprintf('edgemesh_borderwidth %s;',args.edgemesh_borderwidth);
    else
        filestr{end+1} = sprintf('edgemesh_borderwidth %s METRE;',num2str(args.edgemesh_borderwidth));
    end
    filestr{end+1} = sprintf('mesh_convergence_plot %s;',args.mesh_convergence_plot);
    filestr{end+1} = sprintf('save_fields_for %s;',args.save_fields_for);
    filestr{end+1} = sprintf('drcLogging %s;',args.drcLogging);
    filestr{end+1} = sprintf('drcLayer %s;',num2str(args.drcLayer));
    filestr{end+1} = sprintf('overlapextraction %s;',args.overlapExtraction);
    filestr{end+1} = sprintf('gppMergeAllShapes %s;',args.gppMergeAllShapes);
    filestr{end+1} = sprintf('gppSimplifyAbsTol %s LAMBDA;',num2str(args.gppSimplifyAbsTol));
    filestr{end+1} = sprintf('gppSimplifyRelTol %s;',num2str(args.gppSimplifyRelTol));
    filestr{end+1} = sprintf('gppMinFeatureSize %s;',num2str(args.gppMinFeatureSize));
    filestr{end+1} = sprintf('topoWireViasPadringRadius %s VIARADII;',num2str(args.topoWireViasPadringRadius));
    filestr{end+1} = sprintf('topoWireViasAntipadringRadius %s VIARADII;',num2str(args.topoWireViasAntipadringRadius));
    filestr{end+1} = sprintf('topoWireViasThermalRadius %s VIARADII;',num2str(args.topoWireViasThermalRadius));
    filestr{end+1} = sprintf('topoWireViasKeepViaOutline %s;',args.topoWireViasKeepThroughPads);
    filestr{end+1} = sprintf('topoWireViasKeepThroughPads %s;',args.topoWireViasKeepThroughPads);
    filestr{end+1} = sprintf('modelTypeStrip %s;',num2str(args.modelTypeStrip));
    filestr{end+1} = sprintf('modelTypeVia %s;',num2str(args.modelTypeVia));
    filestr{end+1} = sprintf('SAVECURRENTSINFILE %s;',num2str(args.saveCurrentsInFile));
    filestr{end+1} = sprintf('DS_DIR "%s";',args.DS_DIR);
    filestr{end+1} = sprintf('DS_NAME "%s";',args.DS_NAME);
    filestr{end+1} = sprintf('REUSEPREVIOUSRESULTS %s;',args.reusePreviousResults);
    filestr{end+1} = sprintf('INCLUDEPORTSOLVER %s;',args.includePortSolver);
    filestr{end+1} = sprintf('matrixSolver %s;',num2str(args.matrixSolver));
end

end

% @generateFunctionHelp 
% @author Adam Cooman
% @institution ELEC, VUB

% @version 21/01/2015 version 1

% @description The function generates a struct with the different
% @description simulation options for the meshing and solving of a momentum
% @description simulation. You can pass it without arguments to obtain a
% @description default struct with the parameters, you can the edit that struct
% @description to the function to write the .opt file
% 

% @tagline generates the default simulation settings for momentum

% @output1 struct with the different default settings
% @outputType1 struct

% @output2 contains the contents of the .opt file that can be used by momentum
% @outputType2 cell array of strings


%% generateFunctionHelp: old help, backed up at 21-Jan-2015. leave this at the end of the function
% ADSgenerateOptFile generates the default simulation settings for momentum
% 
%      [args,filestr] = ADSgenerateOptFile();
%      [args,filestr] = ADSgenerateOptFile('ParamName',paramValue,...);
% 
% The function generates a struct with the different simulation options for the meshing
% and solving of a momentum simulation
% 
% 
% Parameter/Value pairs:
% - 'simulationMode'     default: 1  check: @isscalar
%      
% - 'logFile'     default: 'proj.log'  check: @ischar
%      name of the log file the simulator info is written to
% - 'processPriority'     default: 'Normal'  check: @ischar
%      process priority given to the momentum process. Options are
%      Normal
% - 'mesh_reduction'     default: 'OFF'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      indicates whether mesh reduction is turned ON or OFF while generating
%      the mesh
% - 'no_cells_per_wavelength'     default: 20  check: @isscalar
%      indicates the amount of cells per wavelength
% - 'edgemesh_borderwidth'     default: 0  check: @isscalar
%      width the edge mesh has from the border of the metal planes?
%      The units  are in metre. set it to 0 to have no edge mesh
% - 'mesh_convergence_plot'     default: 'OFF'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      some weird plot that can be made by momentum apparently, no
%      idea what it  does.
% - 'save_fields_for'     default: 'nofrequencies'  check: @ischar
%      
% - 'drcLogging'     default: 'ON'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      turns on the DRC logging. I have no idea what DRC logging does
%      though
% - 'drcLayer'     default: 255  check: @isscalar
%      Layer for the DRC rules
% - 'overlapExtraction'     default: 'ON'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      overlapextraction
% - 'gppMergeAllShapes'     default: 'ON'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      gppMergeAllShapes
% - 'gppSimplifyAbsTol'     default: 0.01  check: @isscalar
%      
% - 'gppSimplifyRelTol'     default: 0.082  check: @isscalar
%      
% - 'gppMinFeatureSize'     default: -0.5  check: @isscalar
%      
% - 'topoWireViasPadringRadius'     default: 3  check: @isscalar
%      
% - 'topoWireViasAntipadringRadius'     default: 5  check: @isscalar
%      
% - 'topoWireViasThermalRadius'     default: 5  check: @isscalar
%      
% - 'topoWireViasKeepViaOutline'     default: 'OFF'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      
% - 'topoWireViasKeepThroughPads'     default: 'OFF'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      
% - 'modelTypeStrip'     default: 3  check: @isscalar
%      
% - 'modelTypeVia'     default: 3  check: @isscalar
%      
% - 'saveCurrentsInFile'     default: 1  check: @isscalar
%      
% - 'DS_DIR'     default: ''  check: @ischar
%      
% - 'DS_NAME'     default: ''  check: @ischar
%      
% - 'reusePreviousResults'     default: 'OFF'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      
% - 'includePortSolver'     default: 'ON'  check: @(x) strcmpi(x,'ON')||strcmpi(x,'OFF')
%      
% - 'matrixSolver'     default: 0  check: @isscalar
%      
% 
% Outputs:
% - args      Type: struct
%      struct with the different default settings
% - filestr      Type: cell array of strings
%      contains the contents of the .opt file that can be used by momentum
% 
% Adam Cooman, ELEC, VUB
% 
% Version info:
%  21/01/2015 version 1
% 
% This documentation was generated with the generateFunctionHelp function at 21-Jan-2015
