function [nodes polygons] = createCORN(structure,polygons,nodes,orientation,up)
%createMCORN.m adds the nodes to the array of nodes and also creates the polygon
%that corresponds to the MCORN structures and adds it to the polygon list.
%Needed fields are:
%   structure   This structure contains the information about the component
%               that is created. In this case a single microstrip line. It has
%               following fields:
%               structure.parameters.Name: Name of the component.
%               structure.parameters.W: Width of the line pair.
%               structure.parameters.Nodes: contains the nodes of the
%               components
%
%   polygons    This array of structures contains the polygons that are
%               already created. It has following fields:
%               polygons(i).Coord: this contains the coordinates of the
%               polygon.
%               polygons(i).Layer: this contains the number of the layer.
%               The toplayer has number 1 or has no number.
%
%   nodes       This struct array contains information about the nodes and
%               has following fields:
%               nodes(i).Name: name of the node
%               nodes(i).x: x-coordinate of the node
%               nodes(i).y: y-coordinate of the node
%               nodes(i).Orientation: the orientation of the nodes
%               expressed in degrees.
%   Orientation To specify orientation of the node
%   Up          1: default/ 0 --> to mirror around x-axis
% The struct arrays nodes and polygons are updated.
%Matthias Caenepeel, ELEC
%28/05/2014 V1

%PATTERN = '\d*\.?\d+';
%This pattern is used to find the value and the unit of the parameters
%using the function regexp: ?<value> will fill in the field value with zero
%or more digits (*) a possible point \.? and one or more d, a space and
%than it fills in the field unit with zero or more word characters \w*
PATTERN2 = '(?<value>\d*\.?\d+)\s*(?<unit>\w*)';  

%Find the parameters from the structure MLIN2
W=regexp(structure.Parameters.W,PATTERN2,'names');
w=str2double(W.value);

switch W.unit
    case 'mm'
        w=w*1e-3;
    case ''
    otherwise
        error('Unit not known')
end


%Check if there is node in the nodes list that corresponds to a node of the MLIN2. If
%there is no node, there is something wrong!

nodeNotFound=1;
i1=1;

while (nodeNotFound)
 
 %find the index of the node in the nodes list that corresponds to a node
 %the MLIN2.
 j1=find(cellfun(@any,regexp({nodes.Name},['^' structure.Nodes{i1} '$'])));
 %choose the node corresponding to that index as the current node
 currentNode=nodes(j1);
 switch(length(currentNode))
     case(1)
         %if a node is found quit the while loop
         nodeNotFound=0;
         %find the index of the node of the MLIN
         nodeIndex=i1;
     case(0)
         if(i1==length(structure.Nodes))
           %no corresponding node in the nodes list
           error('NNF')  
         end
     otherwise 
         error('multiple nodes found')
 end
 i1=i1+1;
end

%initialize the polygon coordinates (it is known that it will be a
%rectangle): thus 4 points with 2 coordinates (a,1)= x coordinate of point
%a, (a,2) = y coordinate of point a
poly=zeros(4,2);

if exist('up','var')
    disp(['Up is ' num2str(up)])
    currentNode.Up=up;
else
	if isfield(structure.Parameters,'up')
		if any(strcmp(structure.Parameters.up,{'0','no','false'}))
			up=0;
            currentNode.Up=up;
		elseif any(strcmp(structure.Parameters.up,{'1','yes','true'}))
			up=1;
            currentNode.Up=up;
		else
			error('the value provided to up cannot be parsed');
		end
	else
		up=1;
	end
	disp(['Up is ' num2str(up)])
end

if exist('orientation','var')
    disp(['Orientation is ' num2str(orientation)])
    currentNode.Orientation=currentNode.Orientation+orientation;
else
	if isfield(structure.Parameters,'orientation')
		disp(['Orientation is ' num2str(structure.Parameters.orientation)])
		currentNode.Orientation=currentNode.Orientation+str2double(structure.Parameters.orientation);
	end
end

theta=currentNode.Orientation*(pi/180);

switch(nodeIndex)
    case(1)
       %choose the reference x,y coordinates
       x=currentNode.x;
       y=currentNode.y;
      
       %update the node, add other node to nodes list
       newNode.Name=structure.Nodes{2};
       
       if isfield(currentNode,'Up')
            if (currentNode.Up==1)
                newNode.x=x+(w/2)*cos(theta)-sin(theta)*(w/2);
                newNode.y=y+(w/2)*sin(theta)+cos(theta)*(w/2);
            else
                newNode.x=x+(w/2)*cos(theta)+sin(theta)*(w/2);
                newNode.y=y+(w/2)*sin(theta)-cos(theta)*(w/2);
            end
       else
           newNode.x=x+(w/2)*cos(theta)-sin(theta)*(w/2);
           newNode.y=y+(w/2)*sin(theta)+cos(theta)*(w/2);
       end
       newNode.Orientation=currentNode.Orientation;
       
     case(2)
       %choose the reference x,y coordinates
       
       
       if isfield(currentNode,'Up')
            if (currentNode.Up==1)
                x=currentNode.x-(w/2)*cos(theta)+(w/2)*sin(theta);
                y=currentNode.y-(w/2)*cos(theta)-(w/2)*sin(theta);
            else
                x=currentNode.x-(w/2)*cos(theta)-(w/2)*sin(theta);
                y=currentNode.y+(w/2)*cos(theta)-(w/2)*sin(theta);
            end
       else
           x=currentNode.x-(w/2)*cos(theta)+(w/2)*sin(theta);
           y=currentNode.y-(w/2)*cos(theta)-(w/2)*sin(theta);
        end
       
       %update the node, add other node to nodes list
       newNode.Name=structure.Nodes{1};
       newNode.x=x;
       newNode.y=y;
       newNode.Orientation=currentNode.Orientation;
end

%calculate coordinates of polygon
poly(1,1)=x-sin(theta)*(w/2);
poly(1,2)=y+cos(theta)*(w/2);

poly(2,1)=x+w*cos(theta)-sin(theta)*(w/2);
poly(2,2)=y+w*sin(theta)+cos(theta)*(w/2);

poly(3,1)=x+w*cos(theta)+sin(theta)*(w/2);
poly(3,2)=y+w*sin(theta)-cos(theta)*(w/2);

poly(4,1)=x+sin(theta)*(w/2);
poly(4,2)=y-cos(theta)*(w/2);

%update polygons array
polygons(end+1)=struct('Coord',poly,'Layer',1);
%update nodes
nodes(j1)=newNode;


end

