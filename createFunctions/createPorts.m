function Ports = createPorts(nodes,netlist)
% createPorts creates an array of Port strucutures from a node list and from the layout. 
%
%   Ports = createPorts(nodes,layout)
%
% The Port structures are necessary to create the proj.pin and proj.prt files.
%
% Needed fields are:
%   nodes       This struct array contains information about the nodes and
%               has following fields:
%               nodes(i).Name: name of the node
%               nodes(i).x: x-coordinate of the node
%               nodes(i).y: y-coordinate of the node
%               nodes(i).Orientation: the orientation of the nodes
%               expressed in degrees.
%
%   layout      This struct array contains the structs in the layout: 
%               MLINS, MCLINS, ports, etc.
%
% It returns a struct array Ports, which contains following fields:
%   Ports(p1).Name: Name of the port
%   Ports(p1).Node: The node to which the port is connected.
%   Ports(p1).Layer: The number of the layer. (Top layer= 1 or '')
%   Ports(p1).x: x-coord of port
%   Ports(p1).y: y-coord of port
%   Ports(p1).Z.real: real part of the termination of the port
%   Ports(p1).Z.imag: imaginary part of the termination of the port
%   Ports(p1).Calibration: calibration type. No calibration: 'None' or ''.
%                          else it is 'TML'.
%   Ports(p1).Net: if the net is not specified use the node name.
%   Ports(p1).plus_pin: optional field: if it is a 2 layered structure
%                       there are two ports that act as one. There is a
%                       plus_pin on the upper layer and minus pin on the
%                       second (GND) layer.
%   Ports(p1).minus_pin: optional field: if it is a 2 layered structure
%                       there are two ports that act as one. There is a
%                       plus_pin on the upper layer and minus pin on the
%                       second (GND) layer.
% TODO: find out what the Net field means. It is important for the XML file
% 'proj.pin' created by writePintText
% 
% Matthias Caenepeel and Adam Cooman, ELEC VUB
% Version info:
%  25/02/2014   version 1
%  12/01/2015   made sure that the function still works if you use Z0 as a parameter to set the characteristic impedance of the ports
%  14/01/2015   If the calibration is not given to the port, TML calibration is used as a default now


% extract the port statements from the netlist
portStatements = netlist(cellfun(@(x) ~isempty(x),regexp({netlist.Type},'^Port$','once')));


for pp=1:length(portStatements)
    % the name of the port is the name of the component
    Ports(pp).Name=portStatements(pp).Name;
    
    % if the second node is ground, ignore it
    if strcmp(portStatements(pp).Nodes{2},'0');
        Ports(pp).Nodes(1).Name=portStatements(pp).Nodes{1};
    else
        Ports(pp).Nodes(1).Name=portStatements(pp).Nodes{1};
        Ports(pp).Nodes(2).Name=portStatements(pp).Nodes{2};
    end
    
    % Add the location of the nodes by looking in the list of nodes
    for nn=1:length(Ports(pp).Nodes)
        ind=find(cellfun(@any,regexp({nodes.Name},['^' Ports(pp).Nodes(nn).Name '$'])));
        if isempty(ind)
            error('Node %s of port is not in nodes list',Ports(pp).Nodes(nn).Name)
        else
            %fill in the x,y-coordinates in the correct field
            Ports(pp).Nodes(nn).x=nodes(ind).x;
            Ports(pp).Nodes(nn).y=nodes(ind).y;
        end
    end
    
    % there are three ways to specify the layers.
    % If the Layer parameter is passed, place all ports on the top layer
    if isfield(portStatements(pp).Parameters,'Layer')
        Ports(pp).Layer = portStatements(pp).Parameters.Layer*ones(1,length(Ports(pp).Nodes));
    else
        % otherwise, look for the Node1Layer and Node2Layer parameters
        if isfield(portStatements(pp).Parameters,'Node1Layer')
            Ports(pp).Layer(1)=portStatements(pp).Parameters.Node1Layer;
        else
            Ports(pp).Layer(1)=1;
        end
        if isfield(portStatements(pp).Parameters,'Node2Layer')
            % only use this parameter if the port is connected between two nodes
            if length(Ports(pp).Nodes)==2
                Ports(pp).Layer(2)=str2double(portStatements(pp).Parameters.Node2Layer);
            end
        else
            % the default layer is layer 1
            if length(Ports(pp).Nodes)==2
                Ports(pp).Layer(2)=1;
            end
        end
    end
    
    % I don't know what the Net parameter is used for, but let's allow it
    % to be passed anyway. The default is to use the same as the names of
    % the nodes
    if isfield(portStatements(pp).Parameters,'Net')
        Ports(pp).Parameters.Net=portStatements(pp).Net;
    else
        Ports(pp).Net= Ports(pp).Nodes;
    end
    
    % The Calibration type can be passed in the Calibration parameter
    if isfield(portStatements(pp).Parameters,'Calibration')
        Ports(pp).Calibration=portStatements(pp).Parameters.Calibration;
    else
        Ports(pp).Calibration='TML';
    end
    
    % Now process the impedance
    PATTERN = '(?<value>[+-]?\d*\.?\d+\s*[+-]?\d*\.?\d+\s*[ji]?)\s*(?<unit>\w*)?';
    % I allow both Z and Z0 to be the paramter that sets the port impedance
    if isfield(portStatements(pp).Parameters,'Z')
        Z=regexp(portStatements(pp).Parameters.Z,PATTERN,'names');
    else
        if isfield(portStatements(pp).Parameters,'Z0')
            Z=regexp(portStatements(pp).Parameters.Z0,PATTERN,'names');
        else
            Z.value='50';
        end
    end
    % now that the value string is extracted, evaluate it to obtain the
    % complex number
    Ports(pp).Z.real=real(eval(Z.value));
    Ports(pp).Z.imag=imag(eval(Z.value));
end

% nP=length(Ports);
% for p1=1:nP
%     Ports(p1).PlusPin=Ports(p1).Name;
%     index=find(cellfun(@any,regexp({Ports.Node},['^' Ports(p1).Node '$'])));
%     
%     for i1=1:length(index)
%         if index(i1)~=p1
%             Ports(p1).MinusPin=Ports(index(i1)).Name;
%         else
%             Ports(p1).MinusPin='';
%         end
%     end
% end
       
end


%% Example of a .prt file
% <?xml version="1.0" encoding="UTF-8"?>
% <port_setup version="1.1">
%   <port_list>
%     <port id="1">
%       <name>P1</name>
%       <plus_pin>P1</plus_pin>
%       <minus_pin>P3</minus_pin>
%       <impedance>
%         <real>50</real>
%         <imag>0</imag>
%       </impedance>
%     </port>
%     <port id="2">
%       <name>P2</name>
%       <plus_pin>P2</plus_pin>
%       <minus_pin>P4</minus_pin>
%       <impedance>
%         <real>50</real>
%         <imag>0</imag>
%       </impedance>
%     </port>
%   </port_list>
% </port_setup>

%% example of a .pin file
% <?xml version="1.0" encoding="UTF-8"?>
% <pin_list version="1.0">
%   <!-- note: all coordinates are in meter -->
%   <pin>
%     <name>P1</name>
%     <net>N__1</net>
%     <layout>
%       <shape>
%         <layer>1</layer>
%         <purpose>-1</purpose>
%         <point>
%           <x>0</x>
%           <y>0</y>
%         </point>
%       </shape>
%     </layout>
%   </pin>
%   <pin>
%     <name>P2</name>
%     <net>N__2</net>
%     <layout>
%       <shape>
%         <layer>1</layer>
%         <purpose>-1</purpose>
%         <point>
%           <x>0.042</x>
%           <y>0</y>
%         </point>
%       </shape>
%     </layout>
%   </pin>
%   <pin>
%     <name>P3</name>
%     <net>P3</net>
%     <layout>
%       <shape>
%         <layer>2</layer>
%         <purpose>-1</purpose>
%         <point>
%           <x>0</x>
%           <y>0</y>
%         </point>
%       </shape>
%     </layout>
%   </pin>
%   <pin>
%     <name>P4</name>
%     <net>P4</net>
%     <layout>
%       <shape>
%         <layer>2</layer>
%         <purpose>-1</purpose>
%         <point>
%           <x>0.042</x>
%           <y>0</y>
%         </point>
%       </shape>
%     </layout>
%   </pin>
% </pin_list>

