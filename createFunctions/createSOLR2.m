function [nodes,polygons] = createSOLR2(structure,polygons,nodes,orientation)
%createMCLIN.m adds the nodes to the array of nodes and also creates the polygon
%that corresponds to a square open-loop resonator structure and adds it to the polygon list.
%Needed fields are:
%   structure   This structure contains the information about the component
%               that is created. In this case a coupled line pair. It has
%               following fields:
%               structure.parameters.Name: Name of the component.
%               structure.parameters.A: Length of the line pair.
%               structure.parameters.W: Width of the line pair.
%               structure.parameters.G: Spacings between the line.
%               structure.parameters.Nodes: contains the nodes of the
%               components
%
%   polygons    This array of structures contains the polygons that are
%               already created. It has following fields:
%               polygons(i).Coord: this contains the coordinates of the
%               polygon.
%               polygons(i).Layer: this contains the number of the layer.
%               The toplayer has number 1 or has no number.
%
%   nodes       This struct array contains information about the nodes and
%               has following fields:
%               nodes(i).Name: name of the node
%               nodes(i).x: x-coordinate of the node
%               nodes(i).y: y-coordinate of the node
%               nodes(i).Orientation: the orientation of the nodes
%               expressed in degrees.
% The struct arrays nodes and polygons are updated.
%Matthias Caenepeel, ELEC
%10/01/2015 V1

%PATTERN = '\d*\.?\d+';
%This pattern is used to find the value and the unit of the parameters
%using the function regexp: ?<value> will fill in the field value with zero
%or more digits (*) a possible point \.? and one or more d, a space and
%than it fills in the field unit with zero or more word characters \w*

PATTERN2 = '(?<value>\d*\.?\d+)\s*(?<unit>\w*)';  
PATTERN3 = '(?<value>(\-|\+)?\d*\.?\d+)\s*(?<unit>\w*)';

%Find the parameters from the structure SOLR
W=regexp(structure.Parameters.W,PATTERN2,'names');
w=str2double(W.value);

switch W.unit
    case 'mm'
        w=w*1e-3;
    case ''
    otherwise
        error('Unit W not known')
end

A=regexp(structure.Parameters.A,PATTERN2,'names');
a=str2double(A.value);

switch A.unit
    case 'mm'
        a=a*1e-3;
    case ''
    otherwise
        error('Unit A not known')
end

G=regexp(structure.Parameters.G,PATTERN2,'names');
g=str2double(G.value);

switch G.unit
    case 'mm'
        g=g*1e-3;
    case ''
    otherwise
        error('Unit G not known')
end

OffX=regexp(structure.Parameters.OffX,PATTERN3,'names');
offX=str2double(OffX.value);

switch OffX.unit
    case 'mm'
        offX=offX*1e-3;
    case ''
    otherwise
        error('Unit OffX not known')
end

OffY=regexp(structure.Parameters.OffY,PATTERN3,'names');
offY=str2double(OffY.value);

switch OffY.unit
    case 'mm'
        offY=offY*1e-3;
    case ''
    otherwise
        error('Unit OffY not known')
end

%Check if there is node in the nodes list that corresponds to a node of the CLIN1. If
%there is no node, there is something wrong!

nodeNotFound=1;
i1=1;

% find the index of the node in the nodes list that corresponds to a node
while (nodeNotFound)

 j1=find(cellfun(@any,regexp({nodes.Name},['^' structure.Nodes{i1} '$'])));
 %choose the node corresponding to that index as the current node
 currentNode=nodes(j1);
 switch(length(currentNode))
     case(1)
         %if a node is found quit the while loop
         nodeNotFound=0;
         %find the index of the node of the MLIN
         nodeIndex=i1;
     case(0)
         if(i1==length(structure.Nodes))
           %no corresponding node in the nodes list
           err = MException('MOM:NNF', 'node not found');
           throw(err);
         end
     otherwise 
         error('multiple nodes found')
 end
 i1=i1+1;
end

% if exist('orientation','var')
%     currentNode.Orientation=currentNode.Orientation+orientation;
% end

theta=currentNode.Orientation*(pi/180);

switch(nodeIndex)
    case(1)
       %choose the reference x,y coordinates
       x=cos(theta)*offX-sin(theta)*offY+currentNode.x;
       y=cos(theta)*offY+sin(theta)*offX+currentNode.y; 
       
       newNode1 = currentNode;
       
       newNode.Name=structure.Nodes{2};
       newNode.x=x;
       newNode.y=y;
       newNode.Orientation=currentNode.Orientation;
       
    
end

if exist('orientation','var')
    theta=theta+(orientation)*(pi/180);
else
    if isfield(structure.Parameters,'orientation')
        theta=theta+str2double(structure.Parameters.orientation)*(pi/180);
    end
end

%calculate coordinates of polygons
%polygon 1: upper line
poly(1,1)=x-sin(theta)*(a/2);
poly(1,2)=y+cos(theta)*(a/2);

poly(2,1)=x+cos(theta)*a-sin(theta)*(a/2);
poly(2,2)=y+sin(theta)*a+cos(theta)*(a/2);

poly(3,1)=x+cos(theta)*a-sin(theta)*(g/2);
poly(3,2)=y+sin(theta)*a+cos(theta)*(g/2);

poly(4,1)=x+cos(theta)*(a-w)-sin(theta)*(g/2);
poly(4,2)=y+sin(theta)*(a-w)+cos(theta)*(g/2);

poly(5,1)=x+cos(theta)*(a-w)-sin(theta)*(a/2-w);
poly(5,2)=y+sin(theta)*(a-w)+cos(theta)*(a/2-w);

poly(6,1)=x+cos(theta)*w-sin(theta)*(a/2-w);
poly(6,2)=y+sin(theta)*w+cos(theta)*(a/2-w);

poly(7,1)=x+cos(theta)*w+sin(theta)*(a/2-w);
poly(7,2)=y+sin(theta)*w-cos(theta)*(a/2-w);

poly(8,1)=x+cos(theta)*(a-w)+sin(theta)*(a/2-w);
poly(8,2)=y+sin(theta)*(a-w)-cos(theta)*(a/2-w);

poly(9,1)=x+cos(theta)*(a-w)+sin(theta)*(g/2);
poly(9,2)=y+sin(theta)*(a-w)-cos(theta)*(g/2);

poly(10,1)=x+cos(theta)*a+sin(theta)*(g/2);
poly(10,2)=y+sin(theta)*a-cos(theta)*(g/2);

poly(11,1)=x+cos(theta)*a+sin(theta)*(a/2-w);
poly(11,2)=y+sin(theta)*a-cos(theta)*(a/2-w);

poly(12,1)=x+cos(theta)*(a-w)+sin(theta)*(a/2);
poly(12,2)=y+sin(theta)*(a-w)-cos(theta)*(a/2);

poly(13,1)=x+sin(theta)*(a/2);
poly(13,2)=y-cos(theta)*(a/2);


%update polygons array
polygons(end+1)=struct('Coord',poly,'Layer',1);

%update nodes
nodes(j1)=newNode;
nodes(end+1)=newNode1;



end

