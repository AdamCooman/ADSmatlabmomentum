function netlistStruct = ADSparseNetlist(Netlist)
% The output is a vector of structs which represent the statements in the netlist.
%
% The structures contain the following fields:
%   Name            name of the component, variable or subcircuit
%   Type            name of the master model
%   Nodes           cell array of strings which represent the nodes the element is connected to
%   Parameters      structure which contains the parameters of the component
%   Statements      cell array of structures which contain the statements
%                   in the definition of a subcircuit
%   Value           expression of the value of a variable
%   StatementType   type of statement
%
% In an ADS netlist, the following options are present for each line:
%
%   Instance statement: type [ :name ] node1 ... nodeN [ [ param=value ] ... ]
%       Name        contains the name of the component
%       Type        contains the model of the component
%       Nodes       contains the nodes of the component
%       Parameters  contains a struct with the parameters of the component
%                   and their values
%       Statements  empty
%       Value       empty
%       StatementType   'INSTANCE'
%
%   Variable statement: XXX = expression
%       Name        contains the variable name
%       Type        empty
%       Nodes       empty
%       Parameters  empty
%       Statements  empty
%       Value       contains the value of the variable
%       StatementType   'VARIABLE'
%
%   C preprocessor statement: # followed by text
%       Name        empty
%       Type        empty
%       Nodes       empty
%       Parameters  empty
%       Statements  empty
%       Value       string which contains the C statement
%       StatementType   'ADS_C_PREPROCESSOR'
%
%   Comment: ; followed by text
%       Name        empty
%       Type        empty
%       Nodes       empty
%       Parameters  empty
%       Statements  empty
%       Value       string which contains the comment
%       StatementType   'COMMENT'
%
%   Subcircuit definition: 
%           define subnetworkName ( node1 ... nodeN )
%           [parameters name1 = [value1] ... name n = [value n ] ]
%           elementStatements
%           end [subnetworkName ]
%       Name        name of the defined subcircuit
%       Type        empty
%       Nodes       output nodes of the defined component
%       Parameters  parameters of the component and their default values
%       Statements  mini-netlist of the statements in the definition
%       Value       empty
%       StatementType   'SUBCIRCUIT'
%
%   Model Statement: model name type [ [param = value ] ... ]
%       Name        name of the model
%       Type        type of the original model
%       Nodes       empty
%       Parameters  parameters assigned to the model
%       Statements  empty
%       Value       empty
%       StatementType   'MODEL'
%
%   Global Nodes: globalnode nodename1 [nodename2 ] [... nodenameN ]
%       Name        empty
%       Type        empty
%       Nodes       list of nodes that are declared
%       Parameters  empty
%       Statements  empty
%       Value       empty
%       StatementType   'GLOBALNODE'
%   
%   Options statement: options [ [param = value ] ... ]
%       Name        empty
%       Type        empty
%       Nodes       empty
%       Parameters  parameters and their assigned values for the options
%       Statements  empty
%       Value       empty
%       StatementType   'OPTIONS'
%
% Version history
% 12/02/2014    Version 1
%
% Adam Cooman, ELEC VUB

global nodeDef nameDef parameterDef
% A node name may have any number of letters or digits in it but must not 
% contain any delimiters or non alphanumeric characters. If a node name 
% begins with a digit, then it must consist only of digits.
nodeDef = '([a-zA-Z][a-zA-Z_0-9]*)|([0-9]+)';
% A name may have any number of letters or digits in it but must not contain 
% any delimiters or non alphanumeric characters. The name must begin with a 
% letter or an underscore ( _ ).
nameDef = '[a-zA-Z_][a-zA-Z0-9_]*';
% A parameter field takes the form name = value, where name is a parameter 
% keyword and value is either a numeric expression, the name of a device 
% instance, the name of a model or a character string surrounded by double 
% quotes. Some parameters can be indexed, in which case the name is 
% followed by [i] , [i,j] , or [i,j,k] . i , j , and k must be integer 
% constants or variables.
intOrVar = ['(([0-9]+)|(' nameDef '))'];
parameterDef = [nameDef '(\[' intOrVar '(,' intOrVar ')*\])?'];

% read the text file into a cell array of strings
if ischar(Netlist)
Netlist = readTextFile(Netlist);
elseif iscellstr(Netlist)
    % do nothing
else
    error('The netlist should be a filename or a cell array of strings');
end
 
% in ADS, a long statement can be cut into pieces by adding a \ at the end. remove all those cases
line=1;
while line<length(Netlist)
    t = regexp(Netlist{line},'\\\s*$','once');
    if ~isempty(t)
        Netlist{line} = [Netlist{line}(1:t) Netlist{line+1}];
        Netlist = Netlist(1:length(Netlist)~=line+1);
    else
        line=line+1;
    end
end

% now that we have a netlist without cut-up statements, we can parse.
netlistStruct = parseLines(Netlist);

end

%% function to parse a bunch of lines of a netlist
function res = parseLines(Netlist)
ind = 1;
linenr = 1;
global nameDef
 
while linenr<=length(Netlist)
    % first find out what kind of statement we are dealing with
    linetype = detectLinetype(Netlist{linenr});
    
    switch linetype
        case 'COMMENT'
            res(ind) = parseComment(Netlist{linenr});
            ind = ind+1;
            linenr = linenr+1;
        case 'ADS_C_PREPROCESSOR'
            res(ind) = parseCstatement(Netlist{linenr});
            ind = ind+1;
            linenr = linenr+1;
        case 'VARIABLE'
            res(ind) = parseVariable(Netlist{linenr});
            ind = ind+1;
            linenr = linenr+1;
        case 'INSTANCE'
            res(ind) = parseInstance(Netlist{linenr});
            ind = ind+1;
            linenr = linenr+1;
        case 'MODEL'
            res(ind) = parseModel(Netlist{linenr});
            ind = ind+1;
            linenr = linenr+1;
        case 'GLOBALNODE'
            res(ind) = parseGlobalNode(Netlist{linenr});
            ind = ind+1;
            linenr = linenr+1;
        case 'OPTIONS'
            res(ind) = parseOptions(Netlist{linenr});
            ind = ind+1;
            linenr = linenr+1;
        case 'SUBCIRCUIT'
            % get the name of the subcircuit
            t = regexp(Netlist{linenr},['^define\s+(?<name>'  nameDef ')\s+'],'names');
            % find the end of the subcircuit
            found = 0;
            endlinenr = linenr+1;
            while ~found
                if ~isempty(regexp(Netlist{endlinenr},['^\s*end\s+' t.name],'once'))
                    found = 1;
                else
                    endlinenr = endlinenr+1;
                end
            end
            % pass all the lines of the subcircuit to the parse function
            res(ind) = parseSubcircuit(Netlist(linenr:endlinenr));
            ind = ind+1;
            linenr = endlinenr+1;
        otherwise
            % the line is empty, ignore the line
            linenr = linenr+1;
    end
    
end

end

%% function to detect the type of statement we are dealing with
function type = detectLinetype(line)
global parameterDef nameDef
line = strtrim(line);
if isempty(line)
    type = 'EMPTY';
else
    t=regexp(line,'^(?<start>((;)|(#)|(define)|(model)|(globalnode)|(options)))','names');
    if ~isempty(t)
        switch t.start
            case ';'
                type = 'COMMENT';
            case '#'
                type = 'ADS_C_PREPROCESSOR';
            case 'define'
                type = 'SUBCIRCUIT';
            case 'model'
                type = 'MODEL';
            case 'globalnode'
                type = 'GLOBALNODE';
            case 'options'
                type = 'OPTIONS';
            otherwise
                error('weird error')
        end
    else
        if ~isempty(regexp(line,['^"?' nameDef '"?\s*:'],'once'))
            type = 'INSTANCE';
        elseif ~isempty(regexp(line,['^' parameterDef '\s*='],'once'))
            type = 'VARIABLE';
        else
            error(['Unknown line type encountered: "' line '"']);
        end
    end
end
end

%% function to parse a comment line
function res = parseComment(line)
%   Comment: ; followed by text
%       Name        empty
%       Type        empty
%       Nodes       empty
%       Parameters  empty
%       Statements  empty
%       Value       string which contains the comment
%       StatementType   'COMMENT'
res = genBasicStruct();
res.Value = line(2:end);
res.StatementType = 'COMMENT';
end

%% function to parse a C-preprocessor line
function res = parseCstatement(line)
%   C preprocessor statement: # followed by text
%       Name        empty
%       Type        empty
%       Nodes       empty
%       Parameters  empty
%       Statements  empty
%       Value       string which contains the C statement
%       StatementType   'ADS_C_PREPROCESSOR'
res = genBasicStruct();
res.Value = line(2:end);
res.StatementType = 'ADS_C_PREPROCESSOR';
end

%% function to parse a component statement
function res = parseInstance(line)
%   Instance statement: Master:Name Nodes Parameters
%       Name        contains the name of the component
%       Type        contains the model of the component
%       Nodes       contains the nodes of the component
%       Parameters  contains a struct with the parameters of the component
%                   and their values
%       Statements  empty
%       Value       empty
%       StatementType   'INSTANCE'
global nameDef nodeDef
res = genBasicStruct();

t = regexp(line,['^(?<Type>[a-zA-Z"][a-zA-Z_0-9"]*)\s*:\s*(?<Name>' nameDef ')'],'names');
if ~isempty(t)
    res.Name = t.Name;
    res.Type = t.Type;
else
    error(['cannot parse Statement: "' line '"']);
end

% get the parameters
[Parameters,firstparamname] = extractParameters(line);

% get the nodes out of the statement
t = regexp(line,res.Name,'split');
t = regexp(t{2},firstparamname,'split');
res.Nodes=regexp(t{1},nodeDef,'match');
res.Parameters = Parameters;

res.StatementType='INSTANCE';
end

%% function to parse a subcircuit definition
function res = parseSubcircuit(lines)
%   Subcircuit definition:
%           define subnetworkName ( node1 ... nodeN )
%           [parameters name1 = [value1] ... name n = [value n ] ]
%           elementStatements
%           end [subnetworkName ]
%       Name        name of the defined subcircuit
%       Type        empty
%       Nodes       output nodes of the defined component
%       Parameters  parameters of the component and their default values
%       Statements  mini-netlist of the statements in the definition
%       Value       empty
%       StatementType   'SUBCIRCUIT'
global nameDef nodeDef
res = genBasicStruct();
% get the name out of the define statement
t=regexp(lines{1},['define\s+(?<Name>' nameDef ')\s+'],'names');
if ~isempty(t)
    res.Name = t.Name;
else
    error(['cannot parse first line of the subcircuit definition: "' lines{1} '"']);
end

% get the nodes
t = regexp(lines{1},'(\()|(\))','split');
res.Nodes = regexp(t{2},nodeDef,'match');

if ~isempty(regexp(lines{2},'^parameters\s+','once'))
    res.Parameters=extractParameters(lines{2});
    res.Statements = parseLines(lines(3:end-1));
else
    % there are no parameters
    res.Parameters = struct();
    res.Statements = parseLines(lines(2:end-1));
end
res.StatementType='SUBCIRCUIT';
end

%% function to parse a variable definition
function res = parseVariable(line)
%   Variable statement: XXX = expression
%       Name        contains the variable name
%       Type        empty
%       Nodes       empty
%       Parameters  empty
%       Statements  empty
%       Value       contains the value of the variable
%       StatementType   'VARIABLE'
res = genBasicStruct();
t = regexp(strtrim(line),'\s*=\s*','split');
res.Name = t{1};
res.Value = t{2};
res.StatementType='VARIABLE';
end

%% function to parse model statements
function res = parseModel(line)
%   Model Statement: model name type [ [param = value ] ... ]
%       Name        name of the model
%       Type        type of the original model
%       Nodes       empty
%       Parameters  parameters assigned to the model
%       Statements  empty
%       Value       empty
%       StatementType   'MODEL'
global nameDef
res = genBasicStruct();
t = regexp(strtrim(line),['^model\s+(?<Name>' nameDef ')\s+(?<Type>' nameDef ')\s+'],'names');
if ~isempty(t)
    res.Name = t.Name; 
    res.Type = t.Type;
else
    error(['cannot parse model line: "' line '"']);
end
res.Parameters = extractParameters(line);
res.StatementType='MODEL';

end

%% function to parse global node statements
function res = parseGlobalNode(line)
%   Global Nodes: globalnode nodename1 [nodename2 ] [... nodenameN ]
%       Name        empty
%       Type        empty
%       Nodes       list of nodes that are declared
%       Parameters  empty
%       Statements  empty
%       Value       empty
%       StatementType   'GLOBALNODE'
res = genBasicStruct();
t = regexp(line,'\s+','split');
for ii=2:length(t)
    res.Nodes{ii-1} = t{ii}; % TODO
end
res.StatementType='GLOBALNODE';
end

%% function to parse an options statement
function res = parseOptions(line)
%   Options statement: options [ [param = value ] ... ]
%       Name        empty
%       Type        empty
%       Nodes       empty
%       Parameters  parameters and their assigned values for the options
%       Statements  empty
%       Value       empty
%       StatementType   'OPTIONS'
res = genBasicStruct();
res.Parameters = extractParameters(line);
res.StatementType='OPTIONS';
end

%% utility function to extract parameters and their values from a string
function [paramstruct,firstparamname] = extractParameters(line)
% this function looks for parameter=value pairs and returns them in a struct
global parameterDef
[starts,ends] = regexp(line,[parameterDef '\s*=\s*']);
if ~isempty(starts)
    for ii=1:length(starts)
        % cut the equal sign from the matched string
        param = strtrim(line(starts(ii):ends(ii)));
        param = strtrim(param(1:end-1));
        if ii~=length(starts)
            paramstruct.(genvarnameRev(strtrim(param))) = strtrim(line(ends(ii)+1:starts(ii+1)-1));
        else
            paramstruct.(genvarnameRev(strtrim(param))) = strtrim(line(ends(ii)+1:end));
        end
    end
    firstparamname = line(starts(1):ends(1));
else
    paramstruct = struct;
    firstparamname = '$';
end
end

%% function to initialise the struct
function res = genBasicStruct()
res.Name = '';
res.Type = '';
res.Nodes = {};
res.Parameters = struct();
res.Statements = struct();
res.Value = '';
res.StatementType = '';
end