# Control ADS momentum from Matlab

This toolbox contains a collection of Matlab functions which allow to control momentum simulations in Keysight's Advanced Design System (ADS).

## Usefull functions in this toolbox

The toolbox contains the following important functions:

**ADSparseNetlist** parses an ADS netlist and returns a struct which contains its information.

**ADScreateLayout** generates a circuit lay-out starting from a netlist.

**ADSgenerateSVG** makes a vector graphics image which contains the circuit layout.

**ADSgenerateOptFile** generates the simulation settings for momentum.

**ADSrunMomentum** Low-level function that performs the communication between Matlab and momentum.

**ADSrunEMsimulation** takes a netlist, generates its layout and runs a momentum simulation on the generated layout.

## Example: simulation of a coupled line filter

As a very simple example, we simulate the frequency response of a coupled line filter. The netlist we use is saved in the `CLfilter_demo.net`  file and contains the following components:

```
Port:P1 N1 0 Num=1 Z0=50 Ohm 
MLIN:feed1 N1 N2 W=3.5 mm L=10 mm
MCLIN:coupled1 N2 N3 N4 N5 W=4 mm L=43 mm S=1 mm
MCLIN:coupled2 N4 N6 N7 N8 W=4 mm L=43 mm S=1 mm
MLIN:feed2 N7 N9 W=3.5 mm L=10 mm
Port:P2 N9 0 Num=2 Z0=50 Ohm
```

**TODO:** talk about the substrate in the `proj.ltd`  file.

To run the simulation, we first generate a matlab struct, with the simulation settings, such that we can change them to our needs. Here, we modify the amount of cells per wavelength to 10.

``` matlab
options = ADSgenerateOptFile();
options.no_cells_per_wavelength = 10;
```

With the netlist and the correct options, we are ready to run the simulation:

```matlab
res  = ADSrunEMsimulation('CLfilter_demo.net','showGeneratedLayout',true,...
    'fStart',5.5e9,'fStop',6.5e9,'fStep',50e6,'optSettings',options);
```

With the `showGeneratedLayout`  option set to `true`, The function will plot the generated lay-out in matlab and will also show the used mesh

**TODO:** Show the layout and mesh plot.

The simulation function returns a struct with the simulation results. The struct contains the following fields:

```
res.S	(2x2xF array with the S-parameters of the circuit)
res.Z0 	(2xF matrix with the characteristic impedance obtained during TML calibration)
res.S_Z0 (2x2xF array with the S-parameters normalised in Z0)
res.freq (1xF vector with the list of frequencies)  
```

We plot the obtained S-matrix of the filter:

```matlab
figure(101)
clf
hold on
for uu=1:size(res.S,1);
    for yy=1:size(res.S,2)
        plot(res.freq,db(squeeze(res.S(uu,yy,:))));
    end
end
```

**TODO:** show the result

## How is the lay-out generated

To describe the lay-out of the circuit that is simulated, we use a netlist, in which different components can be interconnected. When we want to interconnect two transmission lines, for example, we will have the following simple netlist

```
Port:Term1 a Z=50 Ohm
MLIN:Line1 a b W=2 mm L= 10 mm
MLIN:Line2 b c W=3 mm L= 10 mm
Port:Term1 c Z=50 Ohm
```

To generate the lay-out associated to this netlist, we start with one of the ports in the netlist. Here, we will start with `Term1` which is connected to node `a` . We assign coordinate (0,0) and an orientation to node `a`  and we then look in the netlist whether any  component is connected to that node. There are three options now:

1. Nothing is connected to node `a` : this is a problem, node a is a floating node.
2. One component is connected to node `a` :  this is nice, we can continue
3. Multiple components are connected to node `a` : also problematic, we throw an error

When we have determined which component is connected to node `a` , we can call its create function. For the MLIN component, the `createMLIN` function is called. It is provided the coordinate of node `a` , the orientation of node `a` and the parameters from the netlist (`W=2 mm`  and `L= 10 mm` ). With this, the create function does the following:

1. It adds its polygons to the list of polygons. In this case, a rectangle with corners (0,-0.5) , (0,0.5) , (10,0.5) , (10,-0.5)
2. It adds any new nodes and their coordinate to the list of nodes. Here, node `b`  is new in the list with coordinate (10,0) and the same orientation as node `a`

When `createMLIN`  is finished, the first line is removed from the list of components and the process is repeated untill all components have generated their lay-out. 