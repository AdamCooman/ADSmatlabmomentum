function [newres,cell] = citistruct_import(filename)


cell = citicell_import(filename,0);

res=struct;
% First, put everything in a struct
for ii=1:length(cell{1})
%     res.(genvarname(cell{1}{ii}{1})).name = cell{1}{ii}{1};
    res.(genvarname(cell{1}{ii}{1})) = processdata(cell{1}{ii}{3},cell{1}{ii}{2});
end

% get the frequency axis out of the cell as well
res.freq = cell{2}{1}{3};

% build the NxNxF matrices that can be present in the struct.
fields = fieldnames(res);
for ii=1:length(fields)
    t = regexp(fields{ii},['^(?<name>[a-zA-Z_0-9]+)' '(0x5B)' '(?<num1>[0-9]+)' '(0x2C)' '(?<num2>[0-9]+)' '(0x5D)' '$'],'names');
    if ~isempty(t)
        % the variable is a matrix, put its values in a new natrix
        newres.(t.name)(str2double(t.num1),str2double(t.num2),:) = res.(fields{ii});
        clear t
        continue
    end
    t = regexp(fields{ii},['^(?<name>[a-zA-Z_0-9]+)' '(0x5B)' '(?<num1>[0-9]+)' '(0x5D)' '$'],'names');
    if ~isempty(t)
        % the variable is a vector, put the value in a new vector
        newres.(t.name)(str2double(t.num1),:) = res.(fields{ii});
        clear t
        continue
    end
    % the value is not a vector nor a matrix, just put it in the new struct
    newres.(fields{ii}) = res.(fields{ii});
    clear t
end
clear res


end

function res = processdata(data,type)

switch upper(type)
    case 'RI'
        res = data(:,1) + 1i*data(:,2);
    case 'MAG'
        res = data;
    otherwise
        error(['unknown data type:' type])
end

end